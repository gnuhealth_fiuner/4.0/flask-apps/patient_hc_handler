# -*- encoding: utf-8 -*-

from flask_wtf import FlaskForm
from wtforms.fields import StringField, PasswordField, SelectField, TextAreaField
from wtforms.validators import Email, DataRequired

## login

class LoginForm(FlaskForm):
    email = StringField('email', id='email_login',
            validators=[DataRequired(), Email()])
    password = PasswordField('Password', id='pwd_login',
            validators=[DataRequired()])
