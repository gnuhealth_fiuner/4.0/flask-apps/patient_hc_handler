# -*- encoding: utf-8 -*-
import os

from app import tryton
from app.handler import blueprint

from flask import send_from_directory, render_template
from app.auth.routes import login_required

from functools import wraps

@blueprint.route('/patient/<pdf_file>')
@tryton.transaction()
@login_required
def get_hc_file(pdf_file):
    path = ""
    filename = pdf_file
    return send_from_directory(path, filename)

@blueprint.route('/patient_index/<pdf_file>')
@tryton.transaction()
@login_required
def patient_index(pdf_file):
    return render_template("patient_index.html",pdf_file=pdf_file)
