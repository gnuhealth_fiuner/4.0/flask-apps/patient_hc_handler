from flask import render_template, request

from app import tryton
from app.base import blueprint

from jinja2 import TemplateNotFound

from trytond.transaction import Transaction

from functools import wraps
from datetime import datetime

from io import BytesIO

@blueprint.route('/')
def index():
    return render_template('/patient_index.html')

@blueprint.route('/<template>')
def route_template(template):
    try:
        if not template.endswith( '.html' ):
            template += '.html'
        # Detect the current page
        segment = get_segment( request )
        # Serve the file (if exists) from app/templates/FILE.html
        return render_template(template,segment=segment)
    except TemplateNotFound:
        return render_template('page-404.html'), 404
    except:
        return render_template('page-500.html'), 500

# Helper - Extract current page name from request 
def get_segment( request ):
    try:
        segment = request.path.split('/')[-1]
        if segment == '':
            segment = 'index'
        return segment
    except:
        return None
